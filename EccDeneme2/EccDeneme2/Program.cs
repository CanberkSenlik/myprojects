﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EccDeneme2
{
    class Program
    {
        private static byte[] publicKey;

        public static byte[] PublicKey { get; internal set; }

        static void Main(string[] args)
        {
            Console.Write("Sifrelenecek veriyi gir :  ");
            string mesaj = Console.ReadLine();

            using (ECDiffieHellmanCng ecc = new ECDiffieHellmanCng())
            {
                ecc.KeyDerivationFunction = ECDiffieHellmanKeyDerivationFunction.Hash;
                ecc.HashAlgorithm = CngAlgorithm.Sha256;
                PublicKey = ecc.PublicKey.ToByteArray();

                Bob bob = new Bob();

                CngKey k = CngKey.Import(bob.PublicKey2, CngKeyBlobFormat.EccPublicBlob);
                byte[] SenderKey = ecc.DeriveKeyMaterial(CngKey.Import(bob.PublicKey2, CngKeyBlobFormat.EccPublicBlob));

                Send(SenderKey, mesaj, out byte[] encryptedMessage, out byte[] IV);
                bob.Receive(encryptedMessage, IV);


            }
        }

        public static void Send(byte[] key, string SecretMessage, out byte[] encryptedMessage, out byte[] IV)
        {
            using (Aes aes = new AesCryptoServiceProvider())
            {
                aes.Key = key;
                IV = aes.IV;
                // encrypt the message
                using (MemoryStream ms = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                {

                    byte[] plainTextMessage = Encoding.UTF8.GetBytes(SecretMessage);
                    cs.Write(plainTextMessage, 0, plainTextMessage.Length);
                    cs.Close();
                    encryptedMessage = ms.ToArray();

                }
            }
        }
    }
}
        public class Bob
        {
            public byte[] PublicKey2;
            private byte[] Key;



            public Bob()
            {
                using (ECDiffieHellmanCng ecc = new ECDiffieHellmanCng())
                {
                    ecc.KeyDerivationFunction = ECDiffieHellmanKeyDerivationFunction.Hash;
                    ecc.HashAlgorithm = CngAlgorithm.Sha256;
                    PublicKey2 = ecc.PublicKey.ToByteArray();
                    Key = ecc.DeriveKeyMaterial(CngKey.Import(EccDeneme2.Program.PublicKey, CngKeyBlobFormat.EccPublicBlob));

                }

                Console.WriteLine(Environment.NewLine + "Encrypted message : " + Environment.NewLine);

                foreach (byte b in Key)
                {

                     Console.Write($"{b}, ");
                      
                }
            }
       public void Receive(byte[] encryptedMessage, byte[] IV)
        {
        Console.WriteLine("--------------------------");
        using (Aes aes = new AesCryptoServiceProvider())
        {
            aes.Key = Key;
            aes.IV = IV;

            //Decryption and show message

            using (MemoryStream ms = new MemoryStream())
                
            {
                using (CryptoStream cs = new CryptoStream(ms,aes.CreateDecryptor(),CryptoStreamMode.Write))
                {
                    cs.Write(encryptedMessage, 0, encryptedMessage.Length);
                    cs.Close();

                    string message = Encoding.UTF8.GetString(ms.ToArray());
                    Console.WriteLine(Environment.NewLine);
                    Console.WriteLine("Decrypted Message");
                    Console.WriteLine(Environment.NewLine + message + Environment.NewLine);

                }
            }
            Console.WriteLine(Environment.NewLine + "Press Key Any Coninue ...");
            Console.ReadKey();

        }
          
        }



           


        }




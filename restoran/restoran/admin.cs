﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace restoran
{
    public partial class admin : Form
    {
        private AnaSayfa anaSayfa = new AnaSayfa();
        public admin()
        {
            InitializeComponent();
        }

        private void bGiris_Click(object sender, EventArgs e)
        {
            Hide();
            anaSayfa.Show();
        }
    }
}

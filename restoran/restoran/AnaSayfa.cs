﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace restoran
{
    public partial class AnaSayfa : Form
    {
        private BinalarForm binalarForm = new BinalarForm();
        public AnaSayfa()
        {
            InitializeComponent();
        }

        private void bBina_Click(object sender, EventArgs e)
        {
            binalarForm.Show();
        }
    }
}

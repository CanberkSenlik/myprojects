﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Yurt
{
    public partial class girisEKle : Form
    {
        public girisEKle()
        {
            InitializeComponent();
        }
        private SqlConnection conn = admin.conn;
        public string updateIds = "";
        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand komut;
            int result;

            string giris = textBox4.Text;
            string ogrenci = textBox1.Text;
            string tarih = textBox2.Text;
            string islem= textBox3.Text;
           
            

            SqlTransaction transaction = conn.BeginTransaction();
            if (updateIds.Length > 0)
                komut = new SqlCommand("update [GirisCikis] set GirisCikisID=@gir,OgrenciID=@ad,TarihSaat=@sd,IslemTuru=@tel where GirisCikis in (" + updateIds + ")", conn, transaction);
            else
                komut = new SqlCommand("Insert into [GirisCikis] (GirisCikisID,OgrenciID,TarihSaat,IslemTuru) values (@gir,@ad, @sd,@tel)", conn, transaction);

            komut.Parameters.AddWithValue("@gir", giris);
            komut.Parameters.AddWithValue("@ad", ogrenci);
            komut.Parameters.AddWithValue("@sd", tarih);
            komut.Parameters.AddWithValue("@tel", islem);

            result = komut.ExecuteNonQuery();
            komut.Dispose();
            transaction.Commit();
            Close();

        }

        private void girisEKle_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yurt
{
    public partial class AnaSayfa : Form
    {
        public AnaSayfa()
        {
            InitializeComponent();
        }

        private void bOgrenci_Click(object sender, EventArgs e)
        {
            (new OgrenciForm()).Show();
        }

        private void bOdemeler_Click(object sender, EventArgs e)
        {
            (new Odemeler()).Show();
        }

        private void bAlacaklar_Click(object sender, EventArgs e)
        {
            (new Alacaklar()).Show();
        }

        private void bGiris_Click(object sender, EventArgs e)
        {
            (new girisCIkis()).Show();
        }

        private void bOda_Click(object sender, EventArgs e)
        {
            (new Odalar()).Show();
        }

        private void bIzinler_Click(object sender, EventArgs e)
        {
            (new Izinler()).Show();
        }

        private void AnaSayfa_Load(object sender, EventArgs e)
        {

        }
    }
}

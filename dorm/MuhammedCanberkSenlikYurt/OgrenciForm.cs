﻿using System;
using System.Data.SqlClient;

using System.Windows.Forms;

namespace Yurt
{
    public partial class OgrenciForm : Form
    {

        private SqlConnection conn = admin.conn;
        public string updateIds = "";
        public OgrenciForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            (new OgrenciEklemeForm()).Show();
        }
        private void refresh()
        {
            SqlTransaction transaction = conn.BeginTransaction();
            SqlCommand command = new SqlCommand("Select  OgrenciID, OgrenciAdi, OgrenciSoyadi,OgrenciTelefon,KayitTarihi,VeliTelefon from [Ogrenciler]", conn, transaction);
            SqlDataReader reader = command.ExecuteReader();
            dataGridView1.Rows.Clear();
            while (reader.Read())
                dataGridView1.Rows.Add(new object[] { reader["OgrenciID"], reader["OgrenciAdi"], reader["OgrenciSoyadi"], reader["OgrenciTelefon"], reader["KayitTarihi"], reader["VeliTelefon"] });
            reader.Dispose();
            command.Dispose();
            transaction.Commit();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void OgrenciForm_Load(object sender, EventArgs e)
        {

        }

        private void bSil_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
                return;
            SqlTransaction transaction = conn.BeginTransaction();
            string ids = "";
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                ids += row.Cells[0].Value.ToString() + ",";
            ids = ids.Remove(ids.Length - 1);
            SqlCommand command = new SqlCommand("delete from [Ogrenciler] where OgrenciID in (" + ids + ")", conn, transaction);
            int result = command.ExecuteNonQuery();
            command.Dispose();
            transaction.Commit();
            refresh();
        }

     //  private void button3_Click(object sender, EventArgs e)
     //  {
     //      if (dataGridView1.SelectedRows.Count == 0)
     //          return;
     //      string ids = "";
     //      foreach (DataGridViewRow row in dataGridView1.SelectedRows)
     //          ids += row.Cells[0].Value.ToString() + ",";
     //      ids = ids.Remove(ids.Length - 1);
     //      OgrenciEklemeForm form = new OgrenciEklemeForm(ids);
     //      form.ShowDialog();
     //      refresh();
     //  }
    }
}

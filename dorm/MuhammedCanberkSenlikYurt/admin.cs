﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Yurt
{
    public partial class admin : Form
    {

        private AnaSayfa anaSayfa = new AnaSayfa();
        public static SqlConnection conn;
        public admin()
        {
            InitializeComponent();
            
            conn = new SqlConnection("Server=DESKTOP-SABNKT9;Database=YurtData;Integrated Security=SSPI;");
            
            
                conn.Open();
            
          
        }

        private void admin_Load(object sender, EventArgs e)
        {

        }

        private void bGiris_Click(object sender, EventArgs e)
        {
            {
                string adminAd = tbKullaniciAdi.Text, sifre = tbSifre.Text;
                SqlTransaction transaction = conn.BeginTransaction();
                SqlCommand command = new SqlCommand("Select TOP(1) '' from [Admin] where [adminAd]=@kullaniciadi and [sifre]=@sifre", conn, transaction);
                command.Parameters.AddWithValue("@kullaniciadi", adminAd);
                command.Parameters.AddWithValue("@sifre", sifre);
                //int result = command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                    Close();
                /*
                while (reader.Read())
                {
                    //Isimler.Add(reader["Adi"]);
                }
                */
                reader.Dispose();
                command.Dispose();
                transaction.Commit();
                anaSayfa.Show();
                Hide();
            }
        }
    }
}

﻿using System;
using System.Data.SqlClient;

using System.Windows.Forms;

namespace Yurt

{
    public partial class OgrenciEklemeForm : Form
    {
        private SqlConnection conn = admin.conn;
        public string updateIds = "";
        public OgrenciEklemeForm()
        {
            InitializeComponent();
        }
       

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            SqlCommand komut;
            int result;
            string ad = tAdi.Text;
            string soyad = tSoyadi.Text;
            string tel = tTelefon.Text;    
            DateTime tarih = dtKayitTarihi.Value;
            string veli = tVeli.Text;




            SqlTransaction transaction = conn.BeginTransaction();
            if (updateIds.Length > 0)
                komut = new SqlCommand("update [Ogrenciler] set  OgrenciAdi=@ad,OgrenciSoyadi=@sd,OgrenciTelefon=@tel,KayitTarihi@tarih,VeliTelefon=@tele where Ogrenciler in (" + updateIds + ")", conn, transaction);
            else
                komut = new SqlCommand("Insert into [Ogrenciler](OgrenciAdi, OgrenciSoyadi,OgrenciTelefon,KayitTarihi,VeliTelefon) values (@ad, @sd,@tel,@tarih,@tele)", conn, transaction);

            komut.Parameters.AddWithValue("@ad", ad);
            komut.Parameters.AddWithValue("@sd", soyad);
            komut.Parameters.AddWithValue("@tel", tel);
            komut.Parameters.AddWithValue("@tele", veli);
            komut.Parameters.AddWithValue("@tarih", tarih);
           
           
            
            result = komut.ExecuteNonQuery();
            komut.Dispose();
            transaction.Commit();
            Close();



        }

        private void OgrenciEklemeForm_Load(object sender, EventArgs e)
        {

        }
    }
    }


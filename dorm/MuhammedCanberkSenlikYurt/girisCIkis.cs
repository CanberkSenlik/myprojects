﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Yurt
{
    public partial class girisCIkis : Form
    {
        public girisCIkis()
        {
            InitializeComponent();
        }
        private SqlConnection conn = admin.conn;
        public string updateIds = "";
        private void bEkle_Click(object sender, EventArgs e)
        {
            (new girisEKle()).Show();
        }
        private void refresh()
        {
            SqlTransaction transaction = conn.BeginTransaction();
            SqlCommand command = new SqlCommand("Select GirisCikisID, OgrenciID, TarihSaat, IslemTuru from [GirisCikis]", conn, transaction);
            SqlDataReader reader = command.ExecuteReader();
            dataGridView1.Rows.Clear();
            while (reader.Read())
                dataGridView1.Rows.Add(new object[] { reader["GirisCikisID"], reader["OgrenciID"], reader["TarihSaat"], reader["IslemTuru"] });
            reader.Dispose();
            command.Dispose();
            transaction.Commit();
        }
        private void girisCIkis_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
                return;
            SqlTransaction transaction = conn.BeginTransaction();
            string ids = "";
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                ids += row.Cells[0].Value.ToString() + ",";
            ids = ids.Remove(ids.Length - 1);
            SqlCommand command = new SqlCommand("delete from [GirisCikis] where OgrenciID in (" + ids + ")", conn, transaction);
            int result = command.ExecuteNonQuery();
            command.Dispose();
            transaction.Commit();
            refresh();

        }
    }
}

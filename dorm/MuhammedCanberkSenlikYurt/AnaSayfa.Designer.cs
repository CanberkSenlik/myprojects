﻿namespace 

{
    partial class AnaSayfa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bGiris = new System.Windows.Forms.Button();
            this.bAlacaklar = new System.Windows.Forms.Button();
            this.bOda = new System.Windows.Forms.Button();
            this.bIzinler = new System.Windows.Forms.Button();
            this.bOdemeler = new System.Windows.Forms.Button();
            this.bOgrenci = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bGiris
            // 
            this.bGiris.Location = new System.Drawing.Point(231, 134);
            this.bGiris.Name = "bGiris";
            this.bGiris.Size = new System.Drawing.Size(94, 98);
            this.bGiris.TabIndex = 0;
            this.bGiris.Text = "Giris/Cikis";
            this.bGiris.UseVisualStyleBackColor = true;
            this.bGiris.Click += new System.EventHandler(this.bGiris_Click);
            // 
            // bAlacaklar
            // 
            this.bAlacaklar.Location = new System.Drawing.Point(231, 30);
            this.bAlacaklar.Name = "bAlacaklar";
            this.bAlacaklar.Size = new System.Drawing.Size(94, 98);
            this.bAlacaklar.TabIndex = 1;
            this.bAlacaklar.Text = "Alacaklar";
            this.bAlacaklar.UseVisualStyleBackColor = true;
            this.bAlacaklar.Click += new System.EventHandler(this.bAlacaklar_Click);
            // 
            // bOda
            // 
            this.bOda.Location = new System.Drawing.Point(31, 134);
            this.bOda.Name = "bOda";
            this.bOda.Size = new System.Drawing.Size(94, 98);
            this.bOda.TabIndex = 2;
            this.bOda.Text = "Oda Islemleri";
            this.bOda.UseVisualStyleBackColor = true;
            this.bOda.Click += new System.EventHandler(this.bOda_Click);
            // 
            // bIzinler
            // 
            this.bIzinler.Location = new System.Drawing.Point(131, 134);
            this.bIzinler.Name = "bIzinler";
            this.bIzinler.Size = new System.Drawing.Size(94, 98);
            this.bIzinler.TabIndex = 3;
            this.bIzinler.Text = "Izinler";
            this.bIzinler.UseVisualStyleBackColor = true;
            this.bIzinler.Click += new System.EventHandler(this.bIzinler_Click);
            // 
            // bOdemeler
            // 
            this.bOdemeler.Location = new System.Drawing.Point(131, 30);
            this.bOdemeler.Name = "bOdemeler";
            this.bOdemeler.Size = new System.Drawing.Size(94, 98);
            this.bOdemeler.TabIndex = 4;
            this.bOdemeler.Text = "Odemeler";
            this.bOdemeler.UseVisualStyleBackColor = true;
            this.bOdemeler.Click += new System.EventHandler(this.bOdemeler_Click);
            // 
            // bOgrenci
            // 
            this.bOgrenci.Location = new System.Drawing.Point(31, 30);
            this.bOgrenci.Name = "bOgrenci";
            this.bOgrenci.Size = new System.Drawing.Size(94, 98);
            this.bOgrenci.TabIndex = 5;
            this.bOgrenci.Text = "Ogrenci Islemleri";
            this.bOgrenci.UseVisualStyleBackColor = true;
            this.bOgrenci.Click += new System.EventHandler(this.bOgrenci_Click);
            // 
            // AnaSayfa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 280);
            this.Controls.Add(this.bOgrenci);
            this.Controls.Add(this.bOdemeler);
            this.Controls.Add(this.bIzinler);
            this.Controls.Add(this.bOda);
            this.Controls.Add(this.bAlacaklar);
            this.Controls.Add(this.bGiris);
            this.Name = "AnaSayfa";
            this.Text = "AnaSayfa";
            this.Load += new System.EventHandler(this.AnaSayfa_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bGiris;
        private System.Windows.Forms.Button bAlacaklar;
        private System.Windows.Forms.Button bOda;
        private System.Windows.Forms.Button bIzinler;
        private System.Windows.Forms.Button bOdemeler;
        private System.Windows.Forms.Button bOgrenci;
    }
}
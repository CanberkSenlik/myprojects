﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace 

{
    public partial class Odalar : Form
    {
        public Odalar()
        {
            InitializeComponent();
        }
        private SqlConnection conn = admin.conn;
        public string updateIds = "";

        private void bEkle_Click(object sender, EventArgs e)
        {
            SqlCommand komut;
            int result;

          
            string ogrenci = textBox1.Text;
            string tarih = textBox2.Text;
         



            SqlTransaction transaction = conn.BeginTransaction();
            if (updateIds.Length > 0)
                komut = new SqlCommand("update [Odalar] set OgrenciID,Kat=@sd,YatakSayisi=@tel where Odalar in (" + updateIds + ")", conn, transaction);
            else
                komut = new SqlCommand("Insert into [Odalar] (Kat,YatakSayisi) values ( @sd,@tel)", conn, transaction);

            komut.Parameters.AddWithValue("@sd", ogrenci);
            komut.Parameters.AddWithValue("@tel", tarih);

            result = komut.ExecuteNonQuery();
            komut.Dispose();
            transaction.Commit();

        }
        private void refresh()
        {
            SqlTransaction transaction = conn.BeginTransaction();
            SqlCommand command = new SqlCommand("Select OdaID, Kat, YatakSayisi from [Odalar]", conn, transaction);
            SqlDataReader reader = command.ExecuteReader();
            dataGridView1.Rows.Clear();
            while (reader.Read())
                dataGridView1.Rows.Add(new object[] { reader["OdaID"], reader["Kat"], reader["YatakSayisi"] });
            reader.Dispose();
            command.Dispose();
            transaction.Commit();
        }
        private void Odalar_Load(object sender, EventArgs e)
        {
            
          
        }

        private void bListele_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void bSil_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
                return;
            SqlTransaction transaction = conn.BeginTransaction();
            string ids = "";
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                ids += row.Cells[0].Value.ToString() + ",";
            ids = ids.Remove(ids.Length - 1);
            SqlCommand command = new SqlCommand("delete from [Odalar] where OdaID in (" + ids + ")", conn, transaction);
            int result = command.ExecuteNonQuery();
            command.Dispose();
            transaction.Commit();
            refresh();
        }
    }
}

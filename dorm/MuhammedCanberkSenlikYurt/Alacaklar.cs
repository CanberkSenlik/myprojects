﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Yurt
{
    public partial class Alacaklar : Form
    {
        public Alacaklar()
        {
            InitializeComponent();
        }
        private SqlConnection conn = admin.conn;
        public string updateIds = "";
        private void bEkle_Click(object sender, EventArgs e)
        {
            SqlCommand komut;
            int result;

            
            string ogrenci = textBox1.Text;
            string tarih = textBox2.Text;
            string islem = textBox3.Text;



            SqlTransaction transaction = conn.BeginTransaction();
            if (updateIds.Length > 0)
                komut = new SqlCommand("update [Alacaklar] set GirisCikisID=@gir,OgrenciID=@ad,TarihSaat=@sd,IslemTuru=@tel where Alacaklar in (" + updateIds + ")", conn, transaction);
            else
                komut = new SqlCommand("Insert into [Alacaklar] (OgrenciID,OgrenciAd,OgrenciTC) values (@ad, @sd,@tel)", conn, transaction);

     
            komut.Parameters.AddWithValue("@ad", ogrenci);
            komut.Parameters.AddWithValue("@sd", tarih);
            komut.Parameters.AddWithValue("@tel", islem);

            result = komut.ExecuteNonQuery();
            komut.Dispose();
            transaction.Commit();
        

        }
        private void refresh()
        {
            SqlTransaction transaction = conn.BeginTransaction();
            SqlCommand command = new SqlCommand("Select alacakID, OgrenciID, OgrenciAd, OgrenciTC from [Alacaklar]", conn, transaction);
            SqlDataReader reader = command.ExecuteReader();
            dataGridView1.Rows.Clear();
            while (reader.Read())
                dataGridView1.Rows.Add(new object[] { reader["alacakID"], reader["OgrenciID"], reader["OgrenciAd"], reader["OgrenciTC"] });
            reader.Dispose();
            command.Dispose();
            transaction.Commit();
        }

        private void Alacaklar_Load(object sender, EventArgs e)
        {
    
        }

        private void button2_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
                return;
            SqlTransaction transaction = conn.BeginTransaction();
            string ids = "";
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                ids += row.Cells[0].Value.ToString() + ",";
            ids = ids.Remove(ids.Length - 1);
            SqlCommand command = new SqlCommand("delete from [GirisCikis] where OgrenciID in (" + ids + ")", conn, transaction);
            int result = command.ExecuteNonQuery();
            command.Dispose();
            transaction.Commit();
            refresh();
        }
    }
}

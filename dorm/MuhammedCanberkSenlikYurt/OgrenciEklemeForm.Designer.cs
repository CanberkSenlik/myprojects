﻿namespace 


{
    partial class OgrenciEklemeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtKayitTarihi = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tVeli = new System.Windows.Forms.TextBox();
            this.tTelefon = new System.Windows.Forms.TextBox();
            this.tSoyadi = new System.Windows.Forms.TextBox();
            this.tAdi = new System.Windows.Forms.TextBox();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dtKayitTarihi
            // 
            this.dtKayitTarihi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.dtKayitTarihi.Location = new System.Drawing.Point(142, 223);
            this.dtKayitTarihi.Margin = new System.Windows.Forms.Padding(2);
            this.dtKayitTarihi.Name = "dtKayitTarihi";
            this.dtKayitTarihi.Size = new System.Drawing.Size(306, 26);
            this.dtKayitTarihi.TabIndex = 66;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(78, 151);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 60;
            this.label3.Text = "Telefon";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(81, 114);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 20);
            this.label4.TabIndex = 59;
            this.label4.Text = "Soyadı";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(54, 223);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 20);
            this.label5.TabIndex = 58;
            this.label5.Text = "Kayıt Tarihi";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.Location = new System.Drawing.Point(49, 188);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 20);
            this.label8.TabIndex = 57;
            this.label8.Text = "Veli Telefon";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.Location = new System.Drawing.Point(105, 80);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 20);
            this.label9.TabIndex = 56;
            this.label9.Text = "Adı";
            // 
            // tVeli
            // 
            this.tVeli.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tVeli.Location = new System.Drawing.Point(142, 188);
            this.tVeli.Margin = new System.Windows.Forms.Padding(4);
            this.tVeli.Multiline = true;
            this.tVeli.Name = "tVeli";
            this.tVeli.Size = new System.Drawing.Size(306, 29);
            this.tVeli.TabIndex = 64;
            // 
            // tTelefon
            // 
            this.tTelefon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tTelefon.Location = new System.Drawing.Point(142, 151);
            this.tTelefon.Margin = new System.Windows.Forms.Padding(4);
            this.tTelefon.Multiline = true;
            this.tTelefon.Name = "tTelefon";
            this.tTelefon.Size = new System.Drawing.Size(306, 29);
            this.tTelefon.TabIndex = 63;
            // 
            // tSoyadi
            // 
            this.tSoyadi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tSoyadi.Location = new System.Drawing.Point(142, 114);
            this.tSoyadi.Margin = new System.Windows.Forms.Padding(4);
            this.tSoyadi.Multiline = true;
            this.tSoyadi.Name = "tSoyadi";
            this.tSoyadi.Size = new System.Drawing.Size(306, 29);
            this.tSoyadi.TabIndex = 61;
            // 
            // tAdi
            // 
            this.tAdi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tAdi.Location = new System.Drawing.Point(142, 78);
            this.tAdi.Margin = new System.Windows.Forms.Padding(4);
            this.tAdi.Multiline = true;
            this.tAdi.Name = "tAdi";
            this.tAdi.Size = new System.Drawing.Size(306, 29);
            this.tAdi.TabIndex = 62;
            // 
            // btnKaydet
            // 
            this.btnKaydet.FlatAppearance.BorderSize = 0;
            this.btnKaydet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKaydet.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnKaydet.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnKaydet.ImageIndex = 4;
            this.btnKaydet.Location = new System.Drawing.Point(336, 337);
            this.btnKaydet.Margin = new System.Windows.Forms.Padding(2);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Size = new System.Drawing.Size(112, 61);
            this.btnKaydet.TabIndex = 67;
            this.btnKaydet.Text = "Kaydet";
            this.btnKaydet.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // OgrenciEklemeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 444);
            this.Controls.Add(this.btnKaydet);
            this.Controls.Add(this.dtKayitTarihi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tVeli);
            this.Controls.Add(this.tTelefon);
            this.Controls.Add(this.tSoyadi);
            this.Controls.Add(this.tAdi);
            this.Name = "OgrenciEklemeForm";
            this.Text = "OgrenciEklemeForm";
            this.Load += new System.EventHandler(this.OgrenciEklemeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtKayitTarihi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tVeli;
        private System.Windows.Forms.TextBox tTelefon;
        private System.Windows.Forms.TextBox tSoyadi;
        private System.Windows.Forms.TextBox tAdi;
        private System.Windows.Forms.Button btnKaydet;
    }
}
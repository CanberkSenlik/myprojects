﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace 

{
    public partial class Odemeler : Form
    {

        private SqlConnection conn = admin.conn;
        public string updateIds = "";
        public Odemeler()
        {
            InitializeComponent();
        }

        private void bEkle_Click(object sender, EventArgs e)
        {
            (new OdemeEkle()).Show();
        }
        private void refresh()
        {
            SqlTransaction transaction = conn.BeginTransaction();
            SqlCommand command = new SqlCommand("Select HareketID, OdenenAy, OdemeTutari,OdemeTuru from [Odemeler]", conn, transaction);
            SqlDataReader reader = command.ExecuteReader();
            dataGridView1.Rows.Clear();
            while (reader.Read())
                dataGridView1.Rows.Add(new object[] { reader["HareketID"], reader["OdenenAy"], reader["OdemeTutari"], reader["OdemeTuru"] });
            reader.Dispose();
            command.Dispose();
            transaction.Commit();
        }
        private void Odemeler_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void bSil_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
                return;
            SqlTransaction transaction = conn.BeginTransaction();
            string ids = "";
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                ids += row.Cells[0].Value.ToString() + ",";
            ids = ids.Remove(ids.Length - 1);
            SqlCommand command = new SqlCommand("delete from [Odemeler] where HareketID in (" + ids + ")", conn, transaction);
            int result = command.ExecuteNonQuery();
            command.Dispose();
            transaction.Commit();
            refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                //      if (dataGridView1.SelectedRows.Count == 0)
                //          return;
                //      string ids = "";
                //      foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                //          ids += row.Cells[0].Value.ToString() + ",";
                //      ids = ids.Remove(ids.Length - 1);
                //      OdemeEkle form = new OdemeEkle(ids);
                //      form.ShowDialog();
                //      refresh();
            }

            //  private void bGuncelle_Click(object sender, EventArgs e)
            // 
            //  }
        }
    }
}

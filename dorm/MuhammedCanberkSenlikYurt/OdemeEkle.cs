﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace 

{
    public partial class OdemeEkle : Form
    {
        private SqlConnection conn = admin.conn;
        public string updateIds = "";
        public OdemeEkle()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bKaydet_Click(object sender, EventArgs e)
        {
            SqlCommand komut;
            int result;
            string hareket = textBox1.Text;
            string odeme = textBox2.Text;
            string odemeTutar = textBox3.Text;
            string odemeTur = comboBox1.Text;
          



            SqlTransaction transaction = conn.BeginTransaction();
            if (updateIds.Length > 0)
                komut = new SqlCommand("update [Odemeler] set  HareketID=@ad,OdenenAy=@sd,OdemeTutari=@tel,OdemeTuru@tarih where Odemeler in (" + updateIds + ")", conn, transaction);
            else
                komut = new SqlCommand("Insert into [Odemeler](HareketID, OdenenAy,OdemeTutari,OdemeTuru) values (@ad, @sd,@tel,@tarih)", conn, transaction);

            komut.Parameters.AddWithValue("@ad", hareket);
            komut.Parameters.AddWithValue("@sd", odeme);
            komut.Parameters.AddWithValue("@tel", odemeTutar);
            komut.Parameters.AddWithValue("@tarih", odemeTur);
       



            result = komut.ExecuteNonQuery();
            komut.Dispose();
            transaction.Commit();
            Close();
           


        }
    }
}

﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Yurt
{
    public partial class Izinler : Form
    {
        private SqlConnection conn = admin.conn;
        public string updateIds = "";
        public Izinler()
        {
            InitializeComponent();
        }
        private void refresh()
        {
            SqlTransaction transaction = conn.BeginTransaction();
            SqlCommand command = new SqlCommand("Select IzinID,OgrenciID,BaslangicTarihi,BitisTarihi,GidilenAdres,SorumluTelefon,Durum from [Izinler]", conn, transaction);
            SqlDataReader reader = command.ExecuteReader();
            dataGridView1.Rows.Clear();
            while (reader.Read())
                dataGridView1.Rows.Add(new object[] { reader["IzinID"], reader["OgrenciID"], reader["BaslangicTarihi"] ,reader["BitisTarihi"], reader["GidilenAdres"], reader["Durum"], reader["SorumluTelefon"] });
            reader.Dispose();
            command.Dispose();
            transaction.Commit();
        }
        private void Izinler_Load(object sender, EventArgs e)
        {

        }

        private void bEkle_Click(object sender, EventArgs e)
        {
            SqlCommand komut;
            int result;


            string ogrenci = textBox1.Text;
           DateTime tarih = dateTimePicker1.Value;
            DateTime bitis = dateTimePicker2.Value;
            string adres = textBox4.Text;
            string telefon = textBox5.Text;
            string durum = textBox6.Text;

            SqlTransaction transaction = conn.BeginTransaction();
            if (updateIds.Length > 0)
                komut = new SqlCommand("update [Izinler] set ogrenci=@ogr,tarih=@tarih,bitis=@bit,adres=@asd,telefon=@sd,Durum=@tel where Izinler in (" + updateIds + ")", conn, transaction);
            else
                komut = new SqlCommand("Insert into [Izinler] (OgrenciID,BaslangicTarihi,BitisTarihi,GidilenAdres,SorumluTelefon,Durum) values (@ogr,@tarih,@bit,@asd, @sd,@tel)", conn, transaction);


            komut.Parameters.AddWithValue("@ogr", ogrenci);
            komut.Parameters.AddWithValue("@tarih", tarih);
            komut.Parameters.AddWithValue("@bit", bitis);
            komut.Parameters.AddWithValue("@asd", adres);
            komut.Parameters.AddWithValue("@sd", telefon);
            komut.Parameters.AddWithValue("@tel", durum);

            result = komut.ExecuteNonQuery();
            komut.Dispose();
            transaction.Commit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
                return;
            SqlTransaction transaction = conn.BeginTransaction();
            string ids = "";
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                ids += row.Cells[0].Value.ToString() + ",";
            ids = ids.Remove(ids.Length - 1);
            SqlCommand command = new SqlCommand("delete from [Izinler] where IzinID in (" + ids + ")", conn, transaction);
            int result = command.ExecuteNonQuery();
            command.Dispose();
            transaction.Commit();
            refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            refresh();
        }
    }
}
